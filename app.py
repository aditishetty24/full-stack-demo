from flask import Flask, render_template, request
from threading import Thread
import requests
import sqlite3

# Development Environment:
# Python 3.7.4
# macOS Mojave - Version 10.14.5

app = Flask(__name__)
hotelResults = {}

# SQLite Database functions


def create_db(file):
    # Creating Database connection
    connection = None
    try:
        connection = sqlite3.connect(file)
        return connection
    except:
        print("ERROR! function: create_db")


def create_table(connection, sqlSchema):
    # Creating table for database
    # Args:
    #   connection: DB connection object
    #   sqlSchema: Table schema to be created
    # Returns:
    #   N/A
    try:
        c = connection.cursor()
        c.execute(sqlSchema)
    except:
        print("ERROR! function: create_table")


def create_entry(connection, data):
    # Creating table for database
    # Args:
    #   connection: DB connection object
    #   data: Data to be stored
    # Returns:
    #   integer -> row ID
    sqlSchema = ''' INSERT INTO hotels(city,checkin,checkout,type,hotels)
              VALUES(?,?,?,?,?) '''
    c = connection.cursor()
    c.execute(sqlSchema, data)
    connection.commit()
    return c.lastrowid


def get_hotels(data):
    # Function to make HTTP POST request
    # Args:
    #   data : request body
    # Returns:
    #    N/A
    global hotelResults
    url = 'https://experimentation.snaptravel.com/interview/hotels'

    try:
        response = requests.post(url, json=data)
        hotelResults[data['provider']] = response.json()['hotels']
    except:
        print("ERROR")

    return True


@app.route('/', methods=['POST', 'GET'])
def hello_world():
    sqlSchema = """ CREATE TABLE IF NOT EXISTS hotels (
                                            id integer PRIMARY KEY AUTOINCREMENT,
                                            city text NOT NULL,
                                            checkin text,
                                            checkout text,
                                            type text NOT NULL,
                                            hotels text
                                        );"""
    # Create database
    connection = create_db("cache.db") # I would prefer memecached/REDIS as a cache for short-term storage

    if connection is not None:
        # Creating table called Hotels
        create_table(connection, sqlSchema)
    else:
        print("ERROR! Unable to create table")

    # Getting input for city, checkin, checkout from HTML form
    cityName = request.form.get('city')
    checkinDate = request.form.get('checkin')
    checkoutDate = request.form.get('checkout')

    # Safeguard against empty fields; Following codde should only execute once form is filled
    if cityName is not None or checkinDate is not None or checkoutDate is not None:
        snaptravelData = {"city": cityName,
                          "checkin": checkinDate,
                          "checkout": checkoutDate,
                          "provider": "snaptravel"}
        hotelslData = {"city": cityName,
                       "checkin": checkinDate,
                       "checkout": checkoutDate,
                       "provider": "retail"}

        # Setting up parallel HTTP POST requests
        loops = []

        threadProcess = Thread(target=get_hotels, args=[snaptravelData])
        threadProcess.start()
        loops.append(threadProcess)

        threadProcess = Thread(target=get_hotels, args=[hotelslData])
        threadProcess.start()
        loops.append(threadProcess)

        for loop in loops:
            loop.join()

        snapResponse = hotelResults['snaptravel']
        hotelsResponse = hotelResults['retail']

        # Storing response to SQLite Database
        snapCache = (cityName, checkinDate, checkoutDate, 'snaptravel', str(snapResponse))
        hotelsCache = (cityName, checkinDate, checkoutDate, 'retail', str(hotelsResponse))

        create_entry(connection, snapCache)
        create_entry(connection, hotelsCache)

        # Transforming snaptravel response for efficient quering
        changedSnapResponse = {}
        for h in snapResponse:
            changedSnapResponse[h['id']] = h

        commonHotels = []

        # Identifying hotels with same ID in both responses
        for hotel in hotelsResponse:# Runtime: O(n)
            hotelID = hotel['id']
            if hotelID in changedSnapResponse:
                h = changedSnapResponse[hotelID] # Runtime: O(1)
                h['hotelsPrice'] = hotel['price']
                commonHotels.append(h)

        # Rendering HTML template with table of hotels
        return render_template('demo.html', name=cityName, data=commonHotels, showTable=1)

    # Rendering HTML template without table (Home Page)
    return render_template('demo.html', name="World", data=[], showTable=0)


if __name__ == '__main__':
    app.run()